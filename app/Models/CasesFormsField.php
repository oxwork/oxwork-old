<?php

namespace Oxwork\Models;

use Illuminate\Database\Eloquent\Model;

class CasesFormsField extends Model
{
    public function form()
    {
        return $this->belongsTo('Oxwork\Models\CasesForms');
    }

    public function type()
    {
        return $this->belongsTo('Oxwork\Models\CasesFormsFieldsType', 'field_code', 'code');
    }
}
