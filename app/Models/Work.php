<?php
namespace Oxwork\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{

    public $fillable = ['title','description'];

}
