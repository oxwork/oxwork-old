<?php

namespace Oxwork\Models;

use Illuminate\Database\Eloquent\Model;

class CasesCategory extends Model
{
    protected $fillable = [
        'title', 'description', 'img',
    ];

    public function cases()
    {
        return $this->hasMany('Oxwork\CasesForm', 'cases_categories_id');
    }
}
