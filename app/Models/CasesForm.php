<?php

namespace Oxwork\Models;

use Illuminate\Database\Eloquent\Model;

class CasesForm extends Model
{
    public function category()
    {
        return $this->belongsTo('Oxwork\Models\CasesCategory', 'cases_categories_id');
    }

    public function fields()
    {
        return $this->hasMany('Oxwork\Models\CasesFormsField', 'forms_id');
    }
}
