<?php

namespace Oxwork\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    public function casesForm()
    {
        return $this->belongsTo('Oxwork\Models\CasesForm', 'cases_forms_id');
    }
}
