<?php

namespace Oxwork\Models;

use Illuminate\Database\Eloquent\Model;

class CasesFormsFieldsType extends Model
{
    public $timestamps = false;
}
