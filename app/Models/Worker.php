<?php

namespace Oxwork\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Worker extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'balance'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
