<?php

namespace Oxwork\Http\Controllers\Worker;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Oxwork\Models\CasesCategory;
use Oxwork\Models\CasesForm;
use Menu;
use Oxwork\Models\Task;
use Oxwork\Http\Controllers\Controller;

class WorkerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.worker');
        Menu::make('MenuWorker', function ($menu) {
            $menu->add('Получить задание', 'worker');
            $menu->add('Мои задания', 'worker/tasks');
        });
    }

    public function index()
    {
        return $this->cases();
    }

    public function cases()
    {
        $cases = CasesForm::all();
        $tasks = Task::all()
            ->where('worker_id', null)
            ->where('status', '=', 0);
        return view('worker.cases')->with(['cases'=> $cases, 'tasks'=> $tasks]);
    }

//  Присваиваем статус для таска
    public function taskComplete ($id)
    {
        $task = Task::all()
                    ->where('id', $id)
                    ->first();

        $task->status = 1;
        $task->save();
        return redirect()->back();
    }

    public function getTask ($id_task)
    {
        $tasks = Task::all()->where('cases_forms_id', $id_task)
                            ->first();
        $tasks->worker_id = Auth::id();
        $tasks->save();
        return redirect('/worker/list');
    }
}
