<?php

namespace Oxwork\Http\Controllers\Worker;

use Oxwork\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/worker';

    protected $redirectAfterLogout = '/worker/login';

    /**
     * LoginController constructor
     */
    public function __construct()
    {
        $this->middleware('auth.worker.guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('worker.login');
    }

    protected function guard()
    {
        return Auth::guard('worker');
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        alert()->success('До свидания!', 'Вы вышли из своего аккаунта.');

        return redirect('/');
    }
}
