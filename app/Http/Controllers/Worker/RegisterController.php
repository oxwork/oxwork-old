<?php

namespace Oxwork\Http\Controllers\Worker;

use Oxwork\Models\Worker;
use Oxwork\Http\Controllers\Controller;

class RegisterController extends Controller
{

    protected $redirectTo = '/worker';
    /**
     * Create a new worker instance
     *
     * @param  array  $data
     * @return Worker
     */
    public static function create(array $data)
    {
        return Worker::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'balance' => 0,
        ]);
    }
}
