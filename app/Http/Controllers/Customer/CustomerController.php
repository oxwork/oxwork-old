<?php

namespace Oxwork\Http\Controllers\Customer;

use Debugbar;
use Menu;
use Oxwork\Http\Controllers\Controller;
use SweetAlert;
use Collective\Html\FormFacade as Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Oxwork\Models\CasesCategory;
use Oxwork\Models\CasesForm;
use Oxwork\Models\CasesFormsFieldsType;
use Oxwork\Models\Task;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        Menu::make('MenuCustomer', function ($menu) {
            $menu->add('Кабинет заказчика', 'customer');
            $menu->add('Поставить задачу', 'customer/cases')->active('customer/cases/*');
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.index');
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function cases()
    {
        $cases = CasesForm::paginate(12);
        return view('customer.cases', ['cases' => $cases]);
    }

    public function casesShow($id)
    {
        $case = CasesForm::find($id);
        if (empty($case)) {
            return view('customer.cases_show', ['case' => null, 'form' => null]);
        }
        $form = '';
        $validation_array = [];
        $form_fields = $case->fields;
        foreach ($form_fields as $form_field) {
            $form_field_type = $form_field->type->type;
            if (method_exists('Collective\Html\FormBuilder', $form_field_type)) {
                $form .= Form::label($id . '-' . $form_field->type->code, $form_field->label, ['class' => 'c-cases-form__label']);
                $form .= Form::$form_field_type($id . '-' . $form_field->type->code);
                $validation = $form_field->type->validation;
                $validation_array[$id . '-' . $form_field->type->code] = $validation;
            };
            $form .= Form::hidden('validation', serialize($validation_array));
        }
        $form .= Form::submit('Отправить задание', ['class' => 'button hollow']);
        Debugbar::info($form);
        return view('customer.cases_show', ['case' => $case, 'form' => $form]);
    }

    public function casesPost(Request $request, $id)
    {
        $validation_array = unserialize($request->get('validation'));
        if (!empty($validation_array)) {
            $this->validate($request, $validation_array);
        }
        $customer_files = [];
        foreach ($request->allFiles() as $key => $file) {
            if ($file->isValid()) {
                $file_path = $file->store('customer');
                $customer_files[$key] = $file_path;
            }
        }
        $task = new Task();
        $task->cases_forms_id = $id;
        $task->customer_reply = json_encode(array_diff($request->all(), $request->allFiles()));
        $task->customer_files = json_encode($customer_files);
        $task->save();
        $message = "Задание успешно создано";
        SweetAlert::message($message);
        //Session::flash('message', $message);
        return redirect()->route('customer.cases');
    }
}
