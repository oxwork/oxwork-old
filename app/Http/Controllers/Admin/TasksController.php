<?php

namespace Oxwork\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Oxwork\Http\Controllers\Controller;
use Oxwork\Models\Task;

class TasksController extends AdminController
{
    public function index()
    {
        $tasks = Task::all();

        foreach ($tasks as $task) {
            $task->casesForm;
            $a[] = json_decode($task->customer_reply);
        }

        dd($a);

        return view('admin.pages.tasks.index', ['tasks' => $tasks]);
    }
}
