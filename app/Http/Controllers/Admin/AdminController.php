<?php

namespace Oxwork\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Oxwork\Http\Controllers\Controller;
use Menu;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
        Menu::make('MenuAdmin', function ($menu) {
            $menu->add('Админка', 'administrator');
            $menu->add('Страницы', ['route'  => 'pages'])
                    ->active('administrator/pages/*');
            $menu->add('Категории', ['route'  => 'categories.index'])
                    ->active('administrator/categories/*');
            $menu->add('Кейсы', ['route'  => 'cases.index'])
                    ->active('administrator/cases/*');
            $menu->add('Задания', ['route' => 'tasks.index'])->active('administrator/tasks/*');
            $menu->add('Настройки', ['route' => 'config.index'])->active('administrator/config/*');
        });
    }

    // Главная страница админки
    public function index()
    {
        return view('admin.index');
    }
}
