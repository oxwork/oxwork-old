<?php

namespace Oxwork\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Oxwork\Http\Controllers\Controller;
use Oxwork\Models\Admin;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;

class ConfigController extends AdminController
{
    public function index()
    {
        $id = Auth::guard('admin')->id();
        $admin = Admin::find($id);

        return view('admin.pages.config.index', ['admin' => $admin]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $result = Admin::where('id', $request->id)->update([
            'email' => $request->email
        ]);

        if ($result) {
            SweetAlert::message('Email успешно изменен');
        } else {
            SweetAlert::message('Ошибка редактирования email');
        }

        return redirect()->back();
    }
}
