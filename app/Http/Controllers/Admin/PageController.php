<?php

namespace Oxwork\Http\Controllers\Admin;

use Oxwork\Http\Controllers\Controller;
use Collective\Html\FormFacade as Form;
use Illuminate\Http\Request;
use Validator;
use Menu;
use Oxwork\Page;
use DB;
use Debugbar;

class PageController extends AdminController
{

    public function index()
    {
        $pages = DB::table('pages')->get();

        return view('admin.pages.page.index', ['pages' => $pages]);
    }

    public function showPage($slug)
    {
        $page = DB::table('pages')->where('slug', '=', $slug)->first();
        return view('admin.pages.page.show', ['page' => $page]);
    }

    /**
     * Form for new page instance
     *
     * @return Page
     */
    public function add()
    {
        $form = $this->getForm();
        return view('admin.pages.page.create', ['form' => $form]);
    }

    /**
     * Form for adding and editing pages
     *
     * @return Form
     */
    public function getForm()
    {
        $form = '';
        $form .= Form::label('title', 'Заголовок страницы');
        $form .= Form::text('title', NULL);
        $form .= Form::label('slug', 'Адрес страницы (slug), не обязательно');
        $form .= Form::text('slug', NULL);
        $form .= Form::label('content', 'Содержимое страницы');
        $form .= Form::textarea('content', NULL);
        $form .= Form::submit('Сохранить', ['class' => 'button']);

        return $form;
    }

    /**
     * Edit page instance
     *
     * @return Page
     */

    public function edit($id)
    {
        $form = $this->getForm();
        $page = DB::table('pages')->where('id', '=', $id)->first();
        return view('admin.pages.page.edit', ['page' => $page, 'form' => $form]);
    }

    /**
     * Update page instance
     *
     * @return Page
     */
    public function update($id, Request $request)
    {
        $page = DB::table('pages')->where('id', '=', $id)->first();

        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'max:100',
            'content' => 'required',
        ]);

        $slug = $this->getSlug($request['slug'], $request['title']);

        $result = DB::table('pages')->where('id', '=', $id)->update(
            ['title' => $request['title'],
                'content' => $request['content'],
                'slug' => $slug
            ]
        );

        if ($result) {
            return redirect('/administrator/pages')->with('message', 'Страница сохранена!');
        } else {
            return back()->withInput()->with('message', 'Что-то пошло не так');
        }
    }

    /**
     * Delete page instance
     *
     * @return Page
     */
    public function destroy($id)
    {
        $page = DB::table('pages')->where('id', '=', $id)->delete();

        return redirect('/administrator/pages')->with('message', 'Страница удалена!');
    }

    /**
     * Add new page instance
     *
     * @return Page
     */
    public function pagesPost(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:pages|max:255',
            'slug' => 'unique:pages|max:100',
            'content' => 'required',
        ]);

        $slug = $this->getSlug($request['slug'], $request['title']);

        $id = DB::table('pages')->insertGetId(
            [
                'title' => $request['title'],
                'content' => $request['content'],
                'slug' => $slug
            ]
        );

        if ($id) {
            return redirect('/administrator/pages')->with('message', 'Страница сохранена!');
        } else {
            return back()->withInput()->with('message', 'Что-то пошло не так');
        }
    }

    public function getSlug($slug, $title)
    {
        if ($slug) {
            $slug = str_slug($slug, '-');
        } else {
            $slug = str_slug($title, '-');
        }

        $page = DB::table('pages')->where('slug', '=', $slug)->first();

        if ($page) {
            $slug = $slug . '-1';
        }

        return $slug;
    }
}
