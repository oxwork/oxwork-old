<?php

namespace Oxwork\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Oxwork\Models\CasesForm;
use Oxwork\Models\CasesCategory;
use Session;
use UxWeb\SweetAlert\SweetAlert;
use Validator;
use Illuminate\Support\Collection;

class CasesController extends AdminController
{
    // Главная страница кейсов
    public function index()
    {
        $collection = new Collection;

        $categories = CasesForm::with('category')->get();
        $cases = CasesForm::all();

        $data = $collection->merge($categories)->merge($cases);

        return view('admin.pages.cases.index', ['cases' => $data]);
    }

    // Страница добавления нового кейса
    public function create()
    {
        $categories = CasesCategory::pluck('title', 'id')->all();

        return view('admin.pages.cases.add', ['categories' => $categories]);
    }

    // Метод добавления нового кейса
    public function store(Request $request)
    {
        $this->validate($request, [
            'cases_categories_id' => 'required|integer',
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'price' => 'required|integer'
        ]);

        $cases = new CasesForm;
        $cases->title = $request->title;
        $cases->cases_categories_id = $request->category;
        $cases->description = $request->description;
        $cases->price = $request->price;
        $cases->save();

        return redirect()->route('cases.index');
    }

    // Страница отображения одного кейса
    public function show($id)
    {
        //
    }

    // Страница редактирвания кейса
    public function edit(Request $request, $id)
    {
        $case = CasesForm::with('category')->find($id);
        $categories = CasesCategory::pluck('title', 'id')->all();

        return view('admin.pages.cases.edit', ['case' => $case, 'categories' => $categories]);
    }

    // Метод обновления кейса в БД
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required|integer',
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'price' => 'required|integer'
        ]);

        $result = CasesForm::where('id', $id)->update([
            'cases_categories_id' => $request->category,
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price,
        ]);

        if ($result) {
            SweetAlert::message('Категория успешно отредактирована');
        } else {
            SweetAlert::message('Ошибка редактирования категории');
        }

        return redirect()->back();
    }

    // Метод удаления кейса из БД
    public function destroy($id)
    {
        $result = CasesForm::destroy($id);

        if ($result) {
            SweetAlert::message('Категория успешно удалена');
        } else {
            SweetAlert::message('Ошибка удаления категории');
        }

        return redirect()->back();
    }
}
