<?php

namespace Oxwork\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Oxwork\Models\CasesCategory;
use Session;
use Validator;
use SweetAlert;

class CasesCategoriesController extends AdminController
{
    // Главная страница категорий кейсов
    public function index()
    {
        $categories = CasesCategory::all();

        return view('admin.pages.category.index', ['categories' => $categories]);
    }

    // Страница добавления новой категории
    public function create()
    {
        return view('admin.pages.category.add');
    }

    // Метод добавления новой категории
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'img' => 'image'
        ]);

        if (!empty($request->file('img'))) {
            $file = $request->file('img');
            $path = 'uploads/images/categories_cases';
            $name = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $file->move($path, $name);
        }

        $category = new CasesCategory;
        $category->title = $request->title;
        $category->description = $request->description;
        $category->img = !empty($name) ? $name: null;
        $category->save();

        return redirect()->route('categories.index');
    }

    // Страница отображения одной категории
    public function show($id)
    {
        // Данный метод запрещен в web.php
    }

    // Страница редактирвания категории
    public function edit($id)
    {
        $category = CasesCategory::find($id);

        return view('admin.pages.category.edit', ['category' => $category]);
    }

    // Метод обновления категории в БД
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'img' => 'image'
        ]);

        if (!empty($request->file('img'))) {
            $file = $request->file('img');
            $name = $file->getClientOriginalName();
            $path = 'uploads/images/categories_cases';
            $file->move($path, $name);
        }

        CasesCategory::where('id', $id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'img' => !empty($name) ? $name : null
        ]);

        return redirect()->route('categories.index');
    }

    // Метод удаления категории из БД
    public function destroy($id)
    {
        $result = CasesCategory::destroy($id);
        if ($result) {
//            Session::flash('message', 'Категория успешно удалена');
            SweetAlert::message('Категория успешно удалена');
        } else {
//            Session::flash('message', 'Ошибка удаления категории');
            SweetAlert::message('Ошибка удаления категории');
        }

        return redirect()->back();
    }
}
