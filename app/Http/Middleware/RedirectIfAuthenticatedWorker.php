<?php
/**
 * Project: oxwork
 * Author: Zorca (vs@zorca.org)
 */

namespace Oxwork\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedWorker
{
    public function handle($request, Closure $next, $guard = 'worker')
    {
        if (Auth::guard($guard)->check()) {
            return redirect('worker');
        }

        return $next($request);
    }
}
