<?php
/**
 * Project: oxwork
 * Author: Zorca (vs@zorca.org)
 */

namespace Oxwork\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Auth;

class AuthenticateAdmin extends Authenticate
{
    public function handle($request, Closure $next, ...$guard)
    {
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('administrator.login');
        }
        return $next($request);
    }
}
