<?php
/**
 * Project: oxwork
 * Author: Zorca (vs@zorca.org)
 */

namespace Oxwork\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Auth;

class AuthenticateWorker extends Authenticate
{
    public function handle($request, Closure $next, ...$guard)
    {
        if (!Auth::guard('worker')->check()) {
            return redirect()->route('worker.login');
        }
        return $next($request);
    }
}
