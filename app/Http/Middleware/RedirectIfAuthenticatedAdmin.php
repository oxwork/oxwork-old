<?php
/**
 * Project: oxwork
 * Author: Zorca (vs@zorca.org)
 */

namespace Oxwork\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedAdmin
{
    public function handle($request, Closure $next, $guard = 'admin')
    {
        if (Auth::guard($guard)->check()) {
            return redirect('administrator');
        }

        return $next($request);
    }
}
