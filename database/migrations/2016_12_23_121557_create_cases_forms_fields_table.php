<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesFormsFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases_forms_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('forms_id')->unsigned();
            $table->foreign('forms_id')
                    ->references('id')
                    ->on('cases_forms')
                    ->onDelete('cascade');
            $table->string('label');
            $table->string('field_code');
            $table->foreign('field_code')->references('code')->on('cases_forms_fields_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases_forms_fields');
    }
}
