<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesFormsFieldsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases_forms_fields_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('code')->unique();
            $table->string('type');
            $table->string('validation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases_forms_fields_types');
    }
}
