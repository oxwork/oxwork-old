<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cases_categories_id')->unsigned();
            $table->foreign('cases_categories_id')
                    ->references('id')
                    ->on('cases_categories')
                    ->onDelete('cascade');
            $table->string('title');
            $table->text('description');
            $table->integer('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases_forms');
    }
}
