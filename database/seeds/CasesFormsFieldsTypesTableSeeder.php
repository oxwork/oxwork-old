<?php

use Illuminate\Database\Seeder;
use Oxwork\Models\CasesFormsFieldsType;

class CasesFormsFieldsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = CasesFormsFieldsType::first();
        if (empty($result)) {
            DB::table('cases_forms_fields_types')->insert([
                [
                    'code' => 'customer_comments',
                    'description' => 'Комментарии заказчика к заданию',
                    'type' => 'textarea',
                    'validation' => '',
                ],
                [
                    'code' => 'text3500',
                    'description' => 'Текст не более 3500 знаков',
                    'type' => 'textarea',
                    'validation' => 'required|max:3500',
                ],
                [
                    'code' => 'psd10',
                    'description' => 'Загрузка PSD-файла размером не более 10 мегабайт',
                    'type' => 'file',
                    'validation' => 'required|max:10240',
                ],
            ]);
        }
    }
}
