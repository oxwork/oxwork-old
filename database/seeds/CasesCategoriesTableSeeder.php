<?php

use Illuminate\Database\Seeder;
use Oxwork\Models\CasesCategory;

class CasesCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = CasesCategory::first();

        if (empty($result)) {
            $category = new CasesCategory;
            $category->title = 'Тексты';
            $category->description = 'Копирайт, рерайт, переводы, коррекция';
            $category->img = 'text.svg';
            $category->save();

            $category = new CasesCategory;
            $category->title = 'Дизайн';
            $category->description = 'Макеты, полиграфия, логотипы, фото';
            $category->img = 'image.svg';
            $category->save();

            $category = new CasesCategory;
            $category->title = 'Сайты';
            $category->description = 'Верстка, программирование, настройка';
            $category->img = 'html.svg';
            $category->save();

            $category = new CasesCategory;
            $category->title = 'Реклама';
            $category->description = 'Продвижение, группы, контекст, SEO';
            $category->img = 'promotion.svg';
            $category->save();
        }
    }
}
