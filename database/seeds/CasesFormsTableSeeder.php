<?php

use Illuminate\Database\Seeder;
use \Oxwork\Models\CasesForm;
use \Illuminate\Support\Facades\DB;

class CasesFormsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $result = CasesForm::first();
        if (empty($result)) {
            DB::table('cases_forms')->insert([
                [
                    'title'=>'Вёрстка из PSD в HTML + CSS',
                    'description'=>'Вёрстка одной страницы из формата PSD(Photoshop) в HTML5 + CSS размером. Размер исходника не более 10 мегабайт.',
                    'price' => 500,
                    'cases_categories_id' => 3,
                ],
                [
                    'title'=>'Фавикон для сайта',
                    'description'=>'Изготовление фавикона для сайта в форматах ico и png.',
                    'price' => 300,
                    'cases_categories_id' => 2,

                ],
                [
                    'title'=>'Перевод с английского на русский',
                    'description'=>'Перевод с английского языка на русский язык текста объемом не более 3500 знаков.',
                    'price' => 400,
                    'cases_categories_id' => 1,
                ],
                [
                    'title'=>'Исправление ошибки Wordpress',
                    'description'=>'Исправление одной ошибки на сайте, под управлением CMS Wordpress.',
                    'price' => 100,
                    'cases_categories_id' => 3,
                ],
            ]);
        } else {
            for ($i=1; $i<=10; $i++) {
                DB::table('cases_forms')->insert([
                    'title'=> $faker->company,
                    'description'=> $faker->text,
                    'price' => $faker->numberBetween(100, 1000),
                    'cases_categories_id' => mt_rand(1, 4),
                ]);
            }
        }
    }
}
