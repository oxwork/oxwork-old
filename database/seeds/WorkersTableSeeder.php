<?php

use Illuminate\Database\Seeder;
use Oxwork\Models\Worker;
use Illuminate\Support\Facades\Hash;

class WorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = Worker::first();
        if (empty($result)) {
            $customer = new Worker;
            $customer->name = 'worker';
            $customer->balance = 500;
            $customer->email = 'worker@worker.ru';
            $customer->password = Hash::make('worker');
            $customer->save();
        }
    }
}
