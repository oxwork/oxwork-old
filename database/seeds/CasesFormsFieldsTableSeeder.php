<?php

use Illuminate\Database\Seeder;
use Oxwork\Models\CasesFormsField;

class CasesFormsFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = CasesFormsField::first();
        if (empty($result)) {
            DB::table('cases_forms_fields')->insert([
                [
                    'forms_id' => 1,
                    'field_code' => 'psd10',
                    'label' => 'Загрузите свой PSD',
                ],
                [
                    'forms_id' => 1,
                    'field_code' => 'customer_comments',
                    'label' => 'Комментарии заказчика к заданию',
                ],
                [
                    'forms_id' => 2,
                    'field_code' => 'customer_comments',
                    'label' => 'Особые пометки',
                ],
                [
                    'forms_id' => 3,
                    'field_code' => 'text3500',
                    'label' => 'Текст для перевода',
                ],
            ]);
        }
    }
}
