<?php

use Illuminate\Database\Seeder;
use Oxwork\Models\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');

        $result = Customer::first();

        if (empty($result)) {
            for ($i=1; $i<=5; $i++) {
                Customer::create([
                    'name' => $faker->name,
                    'balance' => 0,
                    'email' => $faker->email,
                    'password' => Hash::make('customer'),
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time()),
                ]);
            }
        }
    }
}
