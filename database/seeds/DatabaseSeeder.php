<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(WorkersTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(CasesCategoriesTableSeeder::class);
        $this->call(CasesFormsFieldsTypesTableSeeder::class);
        $this->call(CasesFormsTableSeeder::class);
        $this->call(CasesFormsFieldsTableSeeder::class);
    }
}
