<?php

use Illuminate\Database\Seeder;
use Oxwork\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = Admin::first();

        if (empty($result)) {
            $customer = new Admin;
            $customer->name = 'admin';
            $customer->email = 'admin@oxwork.oxwork';
            $customer->password = Hash::make('123456');
            $customer->save();
        }
    }
}
