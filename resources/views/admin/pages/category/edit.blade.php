@extends('layouts.admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <h3 class="panel-heading">Редактирование категории</h3>

            {!! Form::open(['route' => ['categories.update', $category->id], 'method' => 'put', 'files' => true]) !!}
                {!! Form::label('title', 'Заголовок') !!}
                {!! Form::text('title', $category->title, ['placeholder' => 'Название категории']) !!}
                {!! Form::label('description', 'Описание') !!}
                {!! Form::textarea('description', $category->description) !!}
                {!! Form::label('img', 'Миниатюра') !!}
                {!! Form::file('img') !!}
                {!! Form::submit('Обновить', ['class' => 'button']) !!}
            {!! Form::close() !!}

            @if (session()->has('message'))
                <div class="reveal" id="flash-overlay-modal" data-reveal>
                    {!! session('message') !!}
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
      </div>
    </div>
  </div>
@endsection
