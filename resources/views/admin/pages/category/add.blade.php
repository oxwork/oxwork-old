@extends('layouts.admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <h3 class="panel-heading">Добавление новой категории</h3>

            {!! Form::open(['route' => 'categories.store', 'method' => 'post', 'files' => true]) !!}
                {!! Form::label('title', 'Заголовок') !!}
                {!! Form::text('title', old('title'), ['placeholder' => 'Название категории']) !!}
                {!! Form::label('description', 'Описание') !!}
                {!! Form::textarea('description', old('description')) !!}
                {!! Form::label('img', 'Миниатюра') !!}
                {!! Form::file('img') !!}
                {!! Form::submit('Добавить', ['class' => 'button']) !!}
            {!! Form::close() !!}

            @if (session()->has('message'))
                <div class="reveal" id="flash-overlay-modal" data-reveal>
                    {!! session('message') !!}
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
      </div>
    </div>
  </div>
@endsection
