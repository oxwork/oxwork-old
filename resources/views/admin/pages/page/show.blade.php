@extends('layouts.pages')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2>{{$page->title}}</h2>
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection