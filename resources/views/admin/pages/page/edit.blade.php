@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Редактирование страницы</div>
                    <div class="panel-body">

                        {{ Form::model($page, [
                        'method'=> 'PATCH',
                        'route' => ['pages.update', $page->id]
                        ]
                        ) }}
                        {{Form::label('title', 'Заголовок страницы')}}
                        {{Form::text('title', NULL)}}
                        {{Form::label('slug', 'Адрес страницы (slug), не обязательно')}}
                        {{Form::text('slug', NULL)}}
                        {{Form::label('content', 'Содержимое страницы')}}
                        {{Form::textarea('content', NULL)}}
                        {{Form::submit('Сохранить')}}
                        {!! Form::close() !!}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection