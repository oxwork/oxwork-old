@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (!empty($pages))
                            <div class="table-scroll">
                                <table>
                                    <tr><th>Id</th><th>Заголовок</th><th>Адрес</th></tr>
                                    @foreach ($pages as $page)
                                        <tr>
                                            <td>{{$page->id}}</td>
                                            <td>{{$page->title}}</td>
                                            <td>{{link_to($page->slug)}}</td>
                                            <td><a href="{{ route('pages.edit', $page->id) }}" class="success button">Редактировать</a></td>
                                            <td>{!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['pages.destroy', $page->id]
                                                ]) !!}
                                                {!! Form::submit('Удалить', ['class' => 'alert button']) !!}
                                                {!! Form::close() !!}</td></tr>
                                    @endforeach
                                </table>
                            </div>
                        @endif
                        <a class="button button--light" href="pages/add">Добавить страницу</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection