@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <h3 class="panel-heading">Настройки</h3>
                    @if(count($admin) > 0)
                    {!! Form::open(['route' => ['config.update', 'id' => $admin->id], 'method' => 'post']) !!}
                        {!! Form::label('email', 'Почта') !!}
                        {!! Form::text('email', $admin->email, ['placeholder' => 'Почта на которую будут приходить уведомления']) !!}
                        {!! Form::submit('Обновить', ['class' => 'button']) !!}
                    {!! Form::close() !!}
                        {{--<table>--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th width="20">id</th>--}}
                                {{--<th width="10">Миниатюра</th>--}}
                                {{--<th width="150">Заголовок</th>--}}
                                {{--<th colspan="3" width="200">Описание</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}
                            {{--@foreach($categories as $category)--}}
                            {{--<tr>--}}
                            {{--<td>{{ $category->id }}</td>--}}
                            {{--<td><img width="50" src="{{ asset('uploads/images/categories_cases/'.$category->img) }}" alt="{{ $category->title }}"></td>--}}
                            {{--<td>{{ $category->title }}</td>--}}
                            {{--<td>{!! $category->description !!}</td>--}}
                            {{--<td width="100">--}}
                            {{--<a class="button" href="{{ url('administrator/categories/'.$category->id.'/edit') }}">Редактировать</a>--}}
                            {{--</td>--}}
                            {{--<td width="100">--}}
                            {{--<a class="button alert" href="{{ url('administrator/categories/'.$category->id) }}" data-method="delete" data-token="{{csrf_token()}}">Удалить</a>--}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            </tbody>
                        </table>
                    @else
                        <p>Заданий нет</p>
                    @endif

                    @if (session()->has('message'))
                        <div class="reveal" id="flash-overlay-modal" data-reveal>
                            {!! session('message') !!}
                            <button class="close-button" data-close aria-label="Close modal" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
