@extends('layouts.admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <h3 class="panel-heading">Добавление нового кейса</h3>
            {!! Form::open(['route' => 'cases.store', 'method' => 'post', 'files' => true]) !!}
                {!! Form::label('title', 'Заголовок') !!}
                {!! Form::text('title', old('title'), ['placeholder' => 'Название кейса']) !!}
                {!! Form::label('description', 'Описание') !!}
                {!! Form::textarea('description', old('description')) !!}
                {!! Form::label('price', 'Цена') !!}
                {!! Form::text('price', old('title')) !!}
                {!! Form::label('category', 'Категория') !!}
                {!! Form::select('category', $categories) !!}
                {!! Form::submit('Добавить', ['class' => 'button']) !!}
            {!! Form::close() !!}

            @if (session()->has('message'))
                <div class="reveal" id="flash-overlay-modal" data-reveal>
                    {!! session('message') !!}
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if (!empty(count($errors)))
                <div class="reveal" id="flash-overlay-modal" data-reveal>
                    @foreach($errors->all() as $error)
                        {{ $error }}
                        <br>
                    @endforeach
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
      </div>
    </div>
  </div>
@endsection
