@extends('layouts.admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <h3 class="panel-heading">Список кейсов</h3>
          @if(!empty(count($cases)))
            <table>
              <thead>
              <tr>
                <th width="20">id</th>
                <th width="10">Заголовок</th>
                <th width="150">Описание</th>
                <th width="150">Цена</th>
                <th colspan="3" width="200">Категория</th>
              </tr>
              </thead>
              <tbody>
              @foreach($cases as $case)
              <tr>
                  <td>{{ $case->id }}</td>
                  <td><a href="#">{{ $case->title }}</a></td>
                  <td>{!! $case->description !!}</td>
                  <td>{{ $case->price }}</td>
                  <td width="50">
                      <img width="50" src="{{ asset('uploads/images/categories_cases/'.$case->category->img) }}" alt="{{ $case->category->title }}">
                      <p>{{ $case->category->title }}</p>
                      <p><a href="{{ url('administrator/categories/'.$case->category->id.'/edit') }}">Редактировать категорию</a></p>
                  </td>
                  <td>
                      <a class="button prime" href="{{ url('administrator/cases/'.$case->id.'/edit') }}">Редактировать</a>
                      <a class="button alert" href="{{ url('administrator/cases/'.$case->id) }}" data-method="delete" data-token="{{csrf_token()}}">Удалить</a>
                  </td>
              </tr>
                @endforeach
              </tbody>
            </table>
          @else
            <p>Кейсов нет</p>
          @endif
          <a class="button" href="{{ url('administrator/cases/create') }}">Добавить новый кейс</a>
            @if (session()->has('message'))
                <div class="reveal" id="flash-overlay-modal" data-reveal>
                    {!! session('message') !!}
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
      </div>
    </div>
  </div>
@endsection
