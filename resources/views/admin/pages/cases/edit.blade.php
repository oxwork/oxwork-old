@extends('layouts.admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <h3 class="panel-heading">Редактирование кейса</h3>

            {!! Form::open(['route' => ['cases.update', $case->id], 'method' => 'put', 'files' => true]) !!}
                {!! Form::label('title', 'Заголовок') !!}
                {!! Form::text('title', $case->title, ['placeholder' => 'Название категории']) !!}
                {!! Form::label('description', 'Описание') !!}
                {!! Form::textarea('description', $case->description) !!}
                {!! Form::label('price', 'Цена') !!}
                {!! Form::text('price', $case->price) !!}
                {!! Form::label('category', 'Категория') !!}
                {!! Form::select('category', $categories, $case->cases_categories_id) !!}
                {!! Form::submit('Обновить', ['class' => 'button']) !!}
            {!! Form::close() !!}

            @if (session()->has('message'))
                <div class="reveal" id="flash-overlay-modal" data-reveal>
                    {!! session('message') !!}
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
      </div>
    </div>
  </div>
@endsection
