@extends('layouts.app')

@section('content')
  <div class="form-wrapper">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('administrator/login') }}">
      {{ csrf_field() }}
      <div class="form-heading">{{ trans('common.login') }}</div>
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="control-label">{{ trans('common.email_address') }}</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
          <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="control-label">{{ trans('common.password') }}</label>
        <input id="password" type="password" class="form-control" name="password" required>
        @if ($errors->has('password'))
          <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
        @endif
      </div>

      <div class="form-group">
        <div class="">
          <div class="checkbox">
            <label><input type="checkbox" name="remember">{{ trans('common.remember_me') }}</label>
          </div>
        </div>
      </div>

      <div class="form-group text--center">
        <div class="">
          <button type="submit" class="button button--light">{{ trans('common.login_action') }}</button>
        </div>
      </div>
    </form>
  </div>
@endsection
