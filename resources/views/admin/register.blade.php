@extends('layouts.app')

@section('content')
  <div class="form-wrapper">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('administrator/register') }}">
      {{ csrf_field() }}
      <div class="form-heading">{{ trans('common.register') }}</div>
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">{{ trans('common.name') }}</label>
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

        @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">{{ trans('common.email_address') }}</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4 control-label">{{ trans('common.password') }}</label>

        <input id="password" type="password" class="form-control" name="password" required>

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group">
        <label for="password-confirm" class="col-md-4 control-label">{{ trans('common.confirm_password') }}</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
      </div>

      <div class="form-group text--center">
        <button type="submit" class="button button--light">{{ trans('common.register') }}</button>
      </div>
    </form>
  </div>
@endsection
