@extends('layouts.app')

@section('content')
  <div class="l-main">
    <section class="l-section l-section--banner text--center text--white">
      <div class="l-row-column">
        {!! file_get_contents(asset('images/logo-oxwork-banner.svg')) !!}
        <h1 class="text--banner"><span class="text--up text--greeeeen">Oxwork</span> - быстрое решение сложных задач</h1>
        <p class="text--medium text--up text--greeeeen">Более миллиона профессионалов готовы к работе прямо сейчас</p><a class="button alert text--banner text--medium" href="#cases">Поставить задачу</a>
      </div>
    </section>
    <section class="l-section text--center" id="cases">
      <h2 class="text--section-title">Выберите направление вашей задачи</h2>
      <div class="c-frontcats">
        <div class="l-row">
          <div class="l-column small-3">
            <div class="c-frontcats__item">
              <img class="c-frontcats__image" src="{{ asset('uploads/images/categories_cases/text.svg') }}">
              <div class="c-frontcats__title">Тексты</div>
              <p>Копирайт, рерайт, переводы, коррекция</p>
              <a class="button button--light" href="#cases">Выбрать кейс</a>
            </div>
          </div>
          <div class="l-column small-3">
            <div class="c-frontcats__item">
              <img class="c-frontcats__image" src="{{ asset('uploads/images/categories_cases/image.svg') }}">
              <div class="c-frontcats__title">Дизайн</div>
              <p>Макеты, полиграфия, логотипы, фото</p>
              <a class="button button--light" href="#cases">Выбрать кейс</a>
            </div>
          </div>
          <div class="l-column small-3">
            <div class="c-frontcats__item">
              <img class="c-frontcats__image" src="{{ asset('uploads/images/categories_cases/html.svg') }}">
              <div class="c-frontcats__title">Сайты</div>
              <p>Верстка, программирование, настройка</p>
              <a class="button button--light" href="#cases">Выбрать кейс</a>
            </div>
          </div>
          <div class="l-column small-3">
            <div class="c-frontcats__item">
              <img class="c-frontcats__image" src="{{ asset('uploads/images/categories_cases/promotion.svg') }}">
              <div class="c-frontcats__title">Реклама</div>
              <p>Продвижение, группы, контекст, SEO</p>
              <a class="button button--light" href="#cases">Выбрать кейс</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="l-footer">
    <div class="l-footer-top">
      <div class="l-row">
        <div class="l-column small-3">
          <h4 class="text--footer-title">О нас</h4>
          <ul class="list--unstyled">
            <li><a class="link--white" href="/about/">About Us</a></li>
            <li><a class="link--white" href="/press/">Press</a></li>
            <li><a class="link--white" href="/about/careers/">Careers</a></li>
          </ul>
        </div>
        <div class="l-column small-3">
          <h4 class="text--footer-title">Сотрудничество</h4>
          <ul class="list--unstyled">
            <li><a class="link--white" href="/about/">About Us</a></li>
            <li><a class="link--white" href="/press/">Press</a></li>
            <li><a class="link--white" href="/about/careers/">Careers</a></li>
          </ul>
        </div>
        <div class="l-column small-3">
          <h4 class="text--footer-title">Вакансии</h4>
          <ul class="list--unstyled">
            <li><a class="link--white" href="/about/">About Us</a></li>
            <li><a class="link--white" href="/press/">Press</a></li>
            <li><a class="link--white" href="/about/careers/">Careers</a></li>
          </ul>
        </div>
        <div class="l-column small-3">
          <h4 class="text--footer-title">Мы в соцсетях</h4>
          <ul class="list--unstyled">
            <li><a class="link--white" href="/about/">About Us</a></li>
            <li><a class="link--white" href="/press/">Press</a></li>
            <li><a class="link--white" href="/about/careers/">Careers</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="l-footer-bottom">
      <div class="l-row-column">
        <div class="copyright text--center">© 2016 - 2017 Zorca Studio. Этот сайт <i class="fa fa-code"></i> командой с курса LoftSchool</div>
      </div>
    </div>
  </footer>
@endsection
