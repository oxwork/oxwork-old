@extends('layouts.worker')

@section('content')
  <div class="content">
    <h2>Все кейсы</h2>
    <!-- Item Listing -->
    <table class="table table-bordered">
      <thead>
      <tr>
        <th width="200">Кейс</th>
        <th width="150">Сообщить о выполнении</th>
      </tr>
      </thead>
      @forelse($tasks as $task)
        <tr>
          <td>{{ $task->customer_reply }}</td>
          <td>
            <a class="button success" href="/worker/taskComplete/{{$task->id}}">Выполнено</a>
          </td>
        </tr>
      @empty
        <p>Нет заданий</p>
      @endforelse
    </table>
  </div>
@endsection
