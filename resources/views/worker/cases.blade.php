@extends('layouts.worker')

@section('content')
  <div class="" id="manage-vue">
    <h2>Получить задание</h2>
    <p>Выберите кейс и вы получите случайное задание, созданное по шаблону этого кейса.</p>
    <!-- Item Listing -->
    <table class="table table-bordered">
      <thead>
      <tr>
        <th width="200">Кейс</th>
        <th width="150">Всего заданий</th>
        <th width="150">Выбрать кейс</th>
      </tr>
      </thead>
      @forelse($cases as $case)
        <tr>
          <td><a class="" href="/worker/cases/{{ $case->id }}">{{ $case->title }}</a></td>
          <td>{{ $tasks->where('cases_forms_id', $case->id)->count() }}</td>
          <td>
            <a class="button success" href="/worker/cases/{{ $case->id }}">Выбрать кейс</a>
          </td>
        </tr>
      @empty
        <p>Нет заданий</p>
      @endforelse
    </table>
  </div>
@endsection
