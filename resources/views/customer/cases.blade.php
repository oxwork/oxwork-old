@extends('layouts.customer')

@section('user-info')
  Баланс счёта: {{ Auth::user()->balance }} руб.
@endsection

@section('content')
  <ul class="c-cases">
    @foreach ($cases as $case)
      <li class="c-cases__item">
        <div class="c-cases__card">
          <div class="c-cases__card-section">
            <a href="{{ url('customer/cases/' . $case->id) }}">{{ $case->title }}</a>
            <div>Категория: {{ $case->category->title }}</div>
            <div class="c-cases__price">{{ $case->price }} руб.</div>
          </div>
        </div>
      </li>
    @endforeach
  </ul>
  {{ $cases->links() }}
@endsection
