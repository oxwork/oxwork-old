@extends('layouts.customer')

@section('user-info')
  Баланс счёта: {{ Auth::user()->balance }} <i class="fa fa-rub" aria-hidden="true"></i>
@endsection

@section('content')
    @if (!empty($case))
        <div class="c-cases-show">
            <h1>{{ $case->title }}</h1>
            <h2>Описание кейса</h2>
            <p>{{ $case->description }}</p>
            <h2>Стоимость кейса: {{ $case->price }} рублей</h2>
            <div>Категория кейса: {{ $case->category->title }}</div>
            {!! Form::open(['files'=>'true', 'url' => 'customer/cases/' . $case->id, 'class' => 'c-cases-form']) !!}
            {!! $form !!}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::close() !!}
            <a class="button" href="{{ url('customer/cases') }}">Вернуться к списку кейсов</a>
        </div>
    @else
        <div class="c-cases-show">
            Кейc не найден
        </div>
    @endif
@endsection
