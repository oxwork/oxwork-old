<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Oxwork') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/main.css') }}" rel="stylesheet">

  <!-- Scripts -->
  <script>window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?></script>
</head>
<body>
<div data-sticky-container>
  <header class="l-header l-header--border-bottom" data-sticky data-margin-top="0">
    <div class="l-row">
      <div class="l-navbar">
        <div class="l-navbar__left">
          <div class="c-logo">
            <a class="c-logo__link" href="{{ url('/') }}">
              {!! file_get_contents(asset('images/logo-oxwork.svg')) !!}
              <div class="c-logo__text">{{ config('app.name', 'Oxwork') }}</div>
            </a>
          </div>
        </div>
        <div class="l-navbar__right">
          <div class="c-login">
            <div class="c-login__button">
              @if (Auth::guest())
                <ul class="menu">
                  <li><a href="{{ url('login') }}">{{ trans('common.login') }}</a></li>
                  <li><a href="{{ url('register') }}">{{ trans('common.register') }}</a></li>
                </ul>
              @else
                <a href="{{ url('customer/wallet') }}" class="">
                  Баланс счёта: {{ Auth::user()->balance }} <i class="fa fa-rub" aria-hidden="true"></i>
                </a>
                <a href="{{ url('customer') }}" class="">
                  {{ Auth::user()->name }}
                </a>
                <a href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
                <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  @yield('content')
</div>

@include('includes/footer')
</body>
</html>
