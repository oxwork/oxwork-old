<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Oxwork') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/main.css') }}" rel="stylesheet">

  <!-- Scripts -->
  <script>window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?></script>
</head>
<body>
<div data-sticky-container>
  <header class="l-header l-header--border-bottom" data-sticky data-margin-top="0">
    <div class="l-row">
      <div class="l-navbar">
        <div class="l-navbar__left">
          <div class="c-logo">
            <a class="c-logo__link" href="{{ url('/') }}">
              {!! file_get_contents(asset('images/logo-oxwork.svg')) !!}
              <div class="c-logo__text">{{ config('app.name', 'Oxwork') }}</div>
            </a>
          </div>
        </div>
        <div class="l-navbar__right">
          <div class="c-login">
            <div class="c-login__button">
              @if (Auth::guard('admin')->guest())
                <ul class="menu">
                  <li><a href="{{ url('/login') }}">{{ trans('common.login') }}</a></li>
                  <li><a href="{{ url('/register') }}">{{ trans('common.register') }}</a></li>
                </ul>
              @else
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  {{ Auth::guard('admin')->user()->name }}
                </a>
                <a href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  Logout
                </a>
                <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="main main--dashboard">
    <div class="row">
      <div class="medium-8 columns">
        @yield('content')
      </div>
      <div class="medium-4 columns">
        <nav class="navigation card">
          Тут может быть меню <br>
          Не понятно эти страницы доступны только админу или юзерам тоже? <br>
          На данный момент только админу
          {{--{!! Menu::get('MenuPages')->asUl(['class' => 'menu vertical']) !!}--}}
        </nav>
      </div>
    </div>
  </div>
</div>
@if (session()->has('message'))
  <p>{!! session('message') !!}</p>
@endif

@include('includes/footer')
</body>
</html>
