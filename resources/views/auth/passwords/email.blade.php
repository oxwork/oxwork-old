@extends('layouts.app')

<!-- Main Content -->
@section('content')
  <div class="form-wrapper">
    @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
    @endif

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
      {{ csrf_field() }}
      <div class="form-heading">{{ trans('passwords.reset_password') }}</div>
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">{{ trans('common.email_address') }}</label>

        <div class="col-md-6">
          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

          @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="form-group text--center">
        <div class="col-md-6 col-md-offset-4">
          <button type="submit" class="button button--light">
            {{ trans('passwords.send_reset_password_link') }}
          </button>
        </div>
      </div>
    </form>
  </div>
@endsection
