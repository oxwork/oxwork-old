<?php

return [
    'name' => 'Name',
    'email_address' => 'E-Mail Address',
    'password' => 'Password',
    'register' => 'Register',
    'login' => 'Login',
    'login_customer' => 'Login form for customers',
    'login_worker' => 'Login form for workers',
    'login_action' => 'Login',
    'remember-me' => 'Remember Me'
];
