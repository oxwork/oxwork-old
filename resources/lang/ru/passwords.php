<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть длиной не менее 6 символов и подтверждение пароля должно совпадать с самим паролем.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Ссылка для сброса пароля отправлена на Вашу электронную почту!',
    'token' => 'Токен для сброса пароля некорректен.',
    'user' => 'Не найден пользователь с указанным адресом электронной почты.',
    'reset_password' => 'Сброс пароля',
    'send_reset_password_link' => 'Сбросить пароль',

];
