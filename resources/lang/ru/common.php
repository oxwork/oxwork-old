<?php

return [
    'name' => 'Имя',
    'email_address' => 'Электронная почта',
    'password' => 'Пароль',
    'register' => 'Регистрация',
    'login' => 'Вход',
    'login_customer' => 'Форма входа для заказчиков',
    'login_worker' => 'Форма входа для исполнителей',
    'login_action' => 'Войти',
    'remember_me' => 'Запомнить меня',
    'forgot_password' => 'Забыли свой пароль?',
    'confirm_password' => 'Повторите пароль',
];
