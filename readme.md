# Биржа фриланса нового поколения Oxwork

## Установка
1. Клонировать репозиторий в папку C:\OpenServer\domains\oxwork
2. Установить библиотеки Composer: composer install
3. Настроить в OpenServer домен oxwork на папку C:\OpenServer\domains\oxwork\public
4. Скопировать содержимое .env.local в .env
5. Создать через Adminer базу данных PostgreSQL: oxwork (oxwork/adminer.php)
6. Выполнить миграции и сидирование базы: php artisan migrate и php artisan db:seed
7. Быстро выполнить задание и помочь остальным