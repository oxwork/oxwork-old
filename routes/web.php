<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('main');
});

// Роуты заказчика (customer)
Route::group(['middleware' => ['web']], function () {

    Route::get('/customer', 'Customer\CustomerController@index');
    Route::get('/customer', ['as' => 'customer', 'uses' => 'Customer\CustomerController@index']);
    Route::get('/customer/cases', ['as' => 'customer.cases', 'uses' => 'Customer\CustomerController@cases']);
    Route::get('/customer/cases/{id}', ['as' => 'customer.cases_show', 'uses' => 'Customer\CustomerController@casesShow']);
    Route::post('/customer/cases/{id}', ['as' => 'customer.cases_post', 'uses' => 'Customer\CustomerController@casesPost']);

// Login Routes...
    Route::get('login', ['as' => 'login', 'uses' => 'Customer\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Customer\LoginController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'Customer\LoginController@logout']);

// Registration Routes...
    Route::get('register', ['as' => 'register', 'uses' => 'Customer\RegisterController@showRegistrationForm']);
    Route::post('register', ['as' => 'register.post', 'uses' => 'Customer\RegisterController@register']);

// Password Reset Routes...
    Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Customer\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Customer\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Customer\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Customer\ResetPasswordController@reset']);
});

// Роуты администратора (admin)
Route::group(['prefix' => 'administrator'], function () {
    Route::get('/', ['as' => 'administrator.index', 'uses' => 'Admin\AdminController@index']);
    Route::get('login', ['as' => 'administrator.login','uses' => 'Admin\LoginController@showLoginForm']);
    Route::post('login', ['uses' => 'Admin\LoginController@login']);
    Route::get('logout', ['as' => 'administrator.logout','uses' => 'Admin\LoginController@logout']);

    Route::get('pages', 'Admin\PageController@index');
    Route::get('pages', ['as' => 'pages', 'uses' => 'Admin\PageController@index']);
    Route::get('pages/add', ['as' => 'pages.add', 'uses' => 'Admin\PageController@add']);
    Route::post('pages/add', ['as' => 'pages.store', 'uses' => 'Admin\PageController@pagesPost']);
    Route::get('pages/edit/{id}', ['as' => 'pages.edit', 'uses' => 'Admin\PageController@edit']);
    Route::patch('pages/edit/{id}', ['as' => 'pages.update', 'uses' => 'Admin\PageController@update']);
    Route::delete('pages/delete/{id}', ['as' => 'pages.destroy', 'uses' => 'Admin\PageController@destroy']);

    Route::resource('categories', 'Admin\CasesCategoriesController');
    Route::resource('cases', 'Admin\CasesController');
    Route::get('tasks', ['as' => 'tasks.index', 'uses' => 'Admin\TasksController@index']);
    Route::get('config', ['as' => 'config.index', 'uses' => 'Admin\ConfigController@index']);
    Route::post('config', ['as' => 'config.update', 'uses' => 'Admin\ConfigController@update']);
});

// Роуты исполнителя (customer)
Route::group(['prefix' => 'worker'], function () {

    Route::get('/', 'Worker\WorkerController@index');
    Route::get('/cases', ['as' => 'worker.cases', 'uses' => 'Worker\WorkerController@cases']);
    Route::get('/list/getTask/{id_task}', ['as' => 'worker/getTask', 'uses' => 'Worker\WorkerController@getTask']);
    Route::get('/taskComplete/{id}', ['as' => 'worker/taskComplete', 'uses' => 'Worker\WorkerController@taskComplete']);

    Route::get('/login', ['as' => 'worker.login','uses' => 'Worker\LoginController@showLoginForm']);
    Route::post('/login', ['uses' => 'Worker\LoginController@login']);

    Route::get('/logout', ['as' => 'worker.logout','uses' => 'Worker\LoginController@logout']);
});
